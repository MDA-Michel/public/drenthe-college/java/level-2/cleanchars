public class Main {

    public static String cleanString(String tekst, char[] characters){
        for (int i = 0; i < characters.length; i++) {
            System.out.println(characters[i]);
            tekst = tekst.replace(characters[i], ' ');
        }
        tekst = tekst.replaceAll("\s\s", "\s");
        return tekst;
    }

    public static void main (String[] args){
        char[] characters = {'"', '?', '®', '*'};
        String tekst = "Dit is een tekst met \" en ** en ?? "+
        "en allerlei andere niet wenselijke tekens zoals ® etc. ";

        System.out.println(cleanString(tekst, characters));
    }
}